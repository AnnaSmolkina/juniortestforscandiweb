"use strict"

       
     function DVD (SKU, name, price, size ) {  
       var self=this;
       self.SKU=SKU;
       self.name=name;
       self.price=price;
       self.size=size;
     }
       var dvd1=document.getElementById('Disc1');
       var dvd1= new DVD ("JVC1123", "Acme Disc",1, "700 MB");

       var dvd2=document.getElementById('Disc2');
       var dvd2= new DVD ("JVC1124", "Acme Disc", 1.5, "850 MB");

       var dvd3=document.getElementById('Disc3');
       var dvd3=new DVD ("JVC1125", "Acme Disc",1.75, "1000 MB");

       var dvd4=document.getElementById('Disc4');
       var dvd4= new DVD ("JVC1126", "Acme Disc",2,"1200 MB");
      
       function BOOK (SKU, name, price, weight) {  
        self=this;
       self.SKU=SKU;
       self.name=name;
       self.price=price;
       self.weight=weight;
     }
       var book1= document.getElementById('Book1');
       var book1= new BOOK ("GGPW0072", "War and Peace",20, "2 kg");

       var book2= document.getElementById('Book2');
       var book2= new BOOK ("GGPW0073", "Anna Karenina", 25, "1.8 kg");

       var book3= document.getElementById('Book3');
       var book3= new BOOK ("GGPW0074", "Lolita",22, "1.9 kg");

       var book4= document.getElementById('Book4');
       var book4= new BOOK ("GGPW0075", "The Great Gatsby",23,"1.5 kg");
    
     function FURNITURE (SKU, name, color, price, height, width, length) {  
       self=this;
       self.SKU=SKU;
       self.name=name;
       self.color=color;
       self.price=price;
       self.height=height;
       self.width=width;
       self.length=length;
     }
       var furniture1=document.getElementById('Furniture1');
       var furniture1= new FURNITURE ("TR12005", "Chair Black", "black", 40, "24", "45", "15");
      
       var furniture2=document.getElementById('Furniture2');
       var furniture2= new FURNITURE ("TR12005", "Chair White", "white", 40, "20", "47", "14");

       var furniture3=document.getElementById('Furniture3');
       var furniture3= new FURNITURE ("TR12005", "Chair Red", "red", 40, "26", "44", "16");

       var furniture4=document.getElementById('Furniture4');
       var furniture4= new FURNITURE ("TR12005", "Chair Yellow", "yellow", 40, "27", "40", "12");
   
   
    function gotoIIPage() {
       document.getElementById("IPage").style.display = "none";
       document.getElementById("IIPage").style.display = "block";
        }

        var info1= document.getElementById('JVC1123');
        var info1Value=JVC1123.value; 
        info1.addEventListener ('change', () => showInfoDisc1 ());

        var info2= document.getElementById('JVC1124');
        var info2Value=JVC1124.value; 
        info2.addEventListener ('change', () => showInfoDisc2 ());

        var info3= document.getElementById('JVC1125');
        var info3Value=JVC1125.value; 
        info3.addEventListener ('change', () => showInfoDisc3 ());

        var info4= document.getElementById('JVC1126');
        var info4Value=JVC1126.value; 
        info4.addEventListener ('change', () => showInfoDisc4 ());

        var info6= document.getElementById('GGPW0072');
        var info6Value=GGPW0072.value; 
        info6.addEventListener ('change', () => showInfoBook1 ());
  
        var info7= document.getElementById('GGPW0073');
        var info7Value=GGPW0073.value; 
        info7.addEventListener ('change', () => showInfoBook2 ());

        var info8= document.getElementById('GGPW0074');
        var info8Value=GGPW0074.value; 
        info8.addEventListener ('change', () => showInfoBook3 ());

        var info9= document.getElementById('GGPW0075');
        var info9Value=GGPW0075.value; 
        info9.addEventListener ('change', () => showInfoBook4 ());

        var info10= document.getElementById('TR12005');
        var info10Value=TR12005.value; 
        info10.addEventListener ('change', () => showInfoFurniture1 ());

        var info11= document.getElementById('TR12006');
        var info11Value=TR12006.value; 
        info11.addEventListener ('change', () => showInfoFurniture2 ());

        var info12= document.getElementById('TR12007');
        var info12Value=TR12007.value; 
        info12.addEventListener ('change', () => showInfoFurniture3 ());

        var info13= document.getElementById('TR12008');
        var info13Value=TR12008.value; 
        info13.addEventListener ('change', () => showInfoFurniture4 ());

        $('#SizeDVD').change(function () { 
           if ($(this).val() === '1'){
                showInfoDisc1 ();
            }
           if ($(this).val() === '2'){
                showInfoDisc2 ();
           }
            if ($(this).val() === '3'){
                showInfoDisc3 ();
           }
          if ($(this).val() === '4'){
                showInfoDisc4 ();
           }
        });

       $('#wBook').change(function () { 
           if ($(this).val() === '1'){
                showInfoBook4 ();
            }
           if ($(this).val() === '2'){
                showInfoBook2 ();
           }
            if ($(this).val() === '3'){
                showInfoBook3 ();
           }
          if ($(this).val() === '4'){
                showInfoBook1 ();
           }
        });

       $('#color').change(function () { 
           if ($(this).val() === '1'){
                showInfoFurniture1 ();
            }
           if ($(this).val() === '2'){
                 showInfoFurniture2 ();
           }
            if ($(this).val() === '3'){
                 showInfoFurniture3 ();
           }
          if ($(this).val() === '4'){
                showInfoFurniture4 ();
           }
        });
         
       function showInfoDisc1 () {
              document.getElementById("infoDisc1").style.display = "block";
              document.getElementById("infoDisc2").style.display = "none";
              document.getElementById("infoDisc3").style.display = "none";
              document.getElementById("infoDisc4").style.display = "none";
           }

       function showInfoDisc2 () {
              document.getElementById("infoDisc1").style.display = "none";
              document.getElementById("infoDisc2").style.display = "block";
              document.getElementById("infoDisc3").style.display = "none";
              document.getElementById("infoDisc4").style.display = "none";
         }

       function showInfoDisc3 () {
              document.getElementById("infoDisc1").style.display = "none";
              document.getElementById("infoDisc2").style.display = "none";
              document.getElementById("infoDisc3").style.display = "block";
              document.getElementById("infoDisc4").style.display = "none";
           }
     
       function showInfoDisc4 () {
              document.getElementById("infoDisc1").style.display = "none";
              document.getElementById("infoDisc2").style.display = "none";
              document.getElementById("infoDisc3").style.display = "none";
              document.getElementById("infoDisc4").style.display = "block";
        }    
        
       function showInfoBook1 () {
              document.getElementById("infoBook1").style.display = "block";
              document.getElementById("infoBook2").style.display = "none";
              document.getElementById("infoBook3").style.display = "none";
              document.getElementById("infoBook4").style.display = "none";
             }

      function showInfoBook2 () {
              document.getElementById("infoBook1").style.display = "none";
              document.getElementById("infoBook2").style.display = "block";
              document.getElementById("infoBook3").style.display = "none";
              document.getElementById("infoBook4").style.display = "none";
          }

      function showInfoBook3 () {
              document.getElementById("infoBook1").style.display = "none";
              document.getElementById("infoBook2").style.display = "none";
              document.getElementById("infoBook3").style.display = "block";
              document.getElementById("infoBook4").style.display = "none";
               }
      function showInfoBook4 () {
              document.getElementById("infoBook1").style.display = "none";
              document.getElementById("infoBook2").style.display = "none";
              document.getElementById("infoBook3").style.display = "none";
              document.getElementById("infoBook4").style.display = "block";
             }
      function showInfoFurniture1 () {
              document.getElementById("infoFurniture1").style.display = "block";
              document.getElementById("infoFurniture2").style.display = "none";
              document.getElementById("infoFurniture3").style.display = "none";
              document.getElementById("infoFurniture4").style.display = "none";
               var h=document.getElementById('H');
                   h.value= "24";
              var w=document.getElementById('W');
                   w.value= "45";
               var l=document.getElementById('L');
                   l.value= "15";
             }
      function showInfoFurniture2 () {
              document.getElementById("infoFurniture1").style.display = "none";
              document.getElementById("infoFurniture2").style.display = "block";
              document.getElementById("infoFurniture3").style.display = "none";
              document.getElementById("infoFurniture4").style.display = "none";
                var h=document.getElementById('H');
                   h.value= "20";
              var w=document.getElementById('W');
                   w.value= "47";
               var l=document.getElementById('L');
                   l.value= "14";
              }

       function showInfoFurniture3 () {
              document.getElementById("infoFurniture1").style.display = "none";
              document.getElementById("infoFurniture2").style.display = "none";
              document.getElementById("infoFurniture3").style.display = "block";
              document.getElementById("infoFurniture4").style.display = "none";
              var h=document.getElementById('H');
                   h.value= "26";
              var w=document.getElementById('W');
                   w.value= "44";
               var l=document.getElementById('L');
                   l.value= "16";
            }

       function showInfoFurniture4 () {
              document.getElementById("infoFurniture1").style.display = "none";
              document.getElementById("infoFurniture2").style.display = "none";
              document.getElementById("infoFurniture3").style.display = "none";
              document.getElementById("infoFurniture4").style.display = "block";
             var h=document.getElementById('H');
                   h.value = "27";
              var w=document.getElementById('W');
                   w.value = "40";
               var l=document.getElementById('L');
                   l.value = "12";
             }
   
       $('#choice').change(function () { 
           if ($(this).val() === '1'){
                showForm1 ();
            }
           if ($(this).val() === '2'){
               showForm2 ();
           }
            if ($(this).val() === '3'){
               showForm3 ();
           }
        });
            
       function showForm1 () {
              document.getElementById("DescriptionDVD").style.display = "block";
              document.getElementById("DescriptionBook").style.display = "none";
               document.getElementById("DescriptionFurniture").style.display = "none";
             };
 
       function showForm2 () {
              document.getElementById("DescriptionDVD").style.display = "none";
              document.getElementById("DescriptionBook").style.display = "block";
              document.getElementById("DescriptionFurniture").style.display = "none";
             };

       function showForm3 () {
              document.getElementById("DescriptionDVD").style.display = "none";
              document.getElementById("DescriptionBook").style.display = "none";
               document.getElementById("DescriptionFurniture").style.display = "block";
             };


        function gotoIPage() {
           document.getElementById("IIPage").style.display = "none";
           document.getElementById("IPage").style.display = "block";
         }

 
   (function add (selector) {
   
        function save(data) {
         localStorage.setItem(selector, JSON.stringify(data));
       }
    
       function onChange(event) {
            var element = event.target,
            name = element.name,
            value = element.value;
        data[name] = value;
        save(data);
         }
       var elements = document.querySelectorAll(selector),
        data = localStorage.getItem(selector);
    if(data) { 
        data = JSON.parse(data);
    } else {
        save(data = {});
    }
   
    Array.prototype.forEach.call(elements, function(element) {
        var name = element.name,
            value = element.value;
        if(data[name] === value) {
            element.checked = false;
       }
        element.addEventListener("change", onChange);        
    });
 }) ( "input[type=checkbox]");

    
       $('#DELETE' ).click(function() {
          $('input:checked').prop('checked', false);
       });
